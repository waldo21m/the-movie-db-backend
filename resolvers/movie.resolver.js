const movieService = require('../services/movie.service');

const resolvers = {
  Query: {
    moviesList(_, {page}) {
      return movieService.getMoviesList(page);
    },

    movie(_, {id}) {
      return movieService.getMovie(id);
    }
  }
};

module.exports = resolvers;
