const fetch = require('node-fetch');
require("dotenv").config();

/**
 * Fetch movies list data.
 *
 * @param page - Page for the paginator.
 *
 * @returns The movies list paginated.
 */
const getMoviesList = async (page = 1) => {
  const url = `${process.env.BASE_URL}/popular?page=${page}`;
  let response;

  try {
    response = await fetch(url, {
      method: 'GET',
      headers: {Authorization: `Bearer ${process.env.BEARER_TOKEN}`},
    });
  } catch (error) {
    console.log('Get movies list error: ', error.response);
    throw new Error('Get movies list error');
  }

  let data;
  try {
    data = await response.json();
  } catch (error) {
    console.log('Parsing movies list to JSON error: ', error.response);
    throw new Error('Parsing movies list to JSON error');
  }

  if (data.errors && data.errors.length) {
    throw new Error(data.errors[0].replace(/^\w/, (c) => c.toUpperCase()));
  }

  const items = data.results.map((item) => {
    return {
      adult: item.adult,
      backdropPath: item.backdrop_path,
      id: item.id,
      originalLanguage: item.original_language,
      originalTitle: item.original_title,
      overview: item.overview,
      posterPath: item.poster_path,
      releaseDate: item.release_date,
      title: item.title,
      video: item.video,
      voteAverage: item.vote_average,
      voteCount: item.vote_count,
    };
  });

  return {
    items,
    page: data.page,
    totalPages: data.total_pages,
    totalResults: data.total_results,
  };
};

/**
 * Fetch movie.
 *
 * @param id - The movie id.
 *
 * @returns The movie.
 */
const getMovie = async (id) => {
  const url = `${process.env.BASE_URL}/${id}`;
  let response;

  try {
    response = await fetch(url, {
      method: 'GET',
      headers: {Authorization: `Bearer ${process.env.BEARER_TOKEN}`},
    });
  } catch (error) {
    console.log('Get movie error: ', error.response);
    throw new Error('Get movie error');
  }

  let data;
  try {
    data = await response.json();
  } catch (error) {
    console.log('Parsing movie to JSON error: ', error.response);
    throw new Error('Parsing movie to JSON error');
  }

  if (data.status_code === 34) {
    throw new Error(data.status_message);
  }

  const productionCompanies = data.production_companies.map((productionCompany) => {
    return {
      id: productionCompany.id,
      logoPath: productionCompany.logoPath,
      name: productionCompany.name,
      originCountry: productionCompany.originCountry,
    };
  });

  const productionCountries = data.production_countries.map((productionCountry) => {
    return {
      iso31661: productionCountry.iso_3166_1,
      name: productionCountry.name,
    };
  });

  return {
    adult: data.adult,
    backdropPath: data.backdrop_path,
    budget: data.budget,
    genres: data.genres,
    homepage: data.homepage,
    id: data.id,
    imdbId: data.imdb_id,
    originalLanguage: data.original_language,
    originalTitle: data.original_title,
    overview: data.overview,
    posterPath: data.poster_path,
    productionCompanies,
    productionCountries,
    releaseDate: data.release_date,
    revenue: data.revenue,
    runtime: data.runtime,
    status: data.status,
    tagline: data.tagline,
    title: data.title,
    video: data.video,
    voteAverage: data.vote_average,
    voteCount: data.vote_count,
  };
};

module.exports = {getMoviesList, getMovie};
