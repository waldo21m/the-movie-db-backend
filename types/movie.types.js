const {gql} = require('apollo-server-express');

/**
 * Movie Schema.
 */
const typeDefs = gql`
  type MovieListResponse {
    items: [Movie]!
    page: Int!
    totalPages: Int!
    totalResults: Int!
  }
  
  type Movie {
    adult: Boolean
    backdropPath: String
    budget: Int
    genres: [Genre]
    homepage: String
    id: ID
    imdbId: ID
    originalLanguage: String
    originalTitle: String
    overview: String
    posterPath: String
    productionCompanies: [ProductionCompany]
    productionCountries: [ProductionCountry]
    releaseDate: String
    revenue: Int
    runtime: Int
    status: String
    tagline: String
    title: String
    video: Boolean
    voteAverage: Float
    voteCount: Int
  }

  type Genre {
    id: ID
    name: String
  }
  
  type ProductionCompany {
    id: ID
    logoPath: String
    name: String
    originCountry: String
  }
  
  type ProductionCountry {
    iso31661: String
    name: String
  }
  
  extend type Query {
    moviesList(page: Int): MovieListResponse!
    movie(id: ID!): Movie
  }
`;

module.exports = typeDefs;
