const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { merge } = require("lodash");
require("dotenv").config();

const movieTypeDefs = require("./types/movie.types");
const movieResolvers = require("./resolvers/movie.resolver");

const app = express();

/**
 * Basic defs.
 */
const typeDefs = `
  type Alert {
    message: String
  }
  
  type Query {
    _ : Boolean
  }
  
  type Mutation {
    _ : Boolean
  }
`;

const resolver = {};

const server = new ApolloServer({
  typeDefs: [typeDefs, movieTypeDefs],
  resolvers: merge(resolver, movieResolvers),
});

server.applyMiddleware({ app });

app.listen(process.env.PORT || 3000, () => console.log("Server initialized"));

module.exports = app;
