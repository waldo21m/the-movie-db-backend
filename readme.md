# 🎬 The Movie DB Backend

The Movie DB es una aplicación para ver las películas más populares.

## 💻 Índice

* [Arquitectura del la aplicación](#archApp)
    * [Librerías implementadas para complementar el desarrollo del Backend](#libraries)
* [Pasos iniciales](#initApp)
* [Ejecutar la aplicación](#execApp)
* [Y mas...](#more)

## <a name="archApp"></a> 🚀 Arquitectura del la aplicación

La aplicación está construida en **NodeJS** usando [GraphQL](https://graphql.org/) gracias a la librería `apollo-server-express`. 

### <a name="libraries"> 📚 Librerías implementadas para complementar el desarrollo del Backend
Podemos destacar algunas de las librerías:
* [Apollo Server Express](https://www.apollographql.com/docs/apollo-server/v1/servers/express/)
* [Dotenv](https://www.npmjs.com/package/dotenv)
* [Express](https://expressjs.com/es/)
* [Lodash](https://lodash.com/)
* [Node Fetch](https://www.npmjs.com/package/node-fetch)

*Nota: Para mayor información del resto de las dependencias, no dude en consultar el archivo package.json.*

## <a name="initApp"></a> 🐾 Pasos iniciales
Una vez clonado el proyecto, debemos movernos a la carpeta raíz, ejecutando el siguiente comando:

```sh
cd the-movie-db-backend
```

Luego necesitamos ejecutar el siguiente comando para instalar todas las librerías necesarias:

```sh
npm i
```

## <a name="execApp"></a> 🤓 Ejecutar la aplicación
Para iniciar la aplicación ejecutamos el siguiente comando:

```sh
npm run start
```

## <a name="more"></a> 💡 Y mas...
Por fines prácticos, los archivos de configuración fueron subidos al repositorio y no son un mero accidente.